/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/04 00:11:09 by tiboitel          #+#    #+#             */
/*   Updated: 2014/12/04 04:12:02 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "generic_array.h"
# define LEAKS 0

int		main(void)
{
	t_garray	*array;
	size_t		i;
	int			magic;
	char 		*string;

	string = ft_strdup("Gangplank");
	magic = 5435; 
	if (!(array = garray_new(9)))
	{
		ft_putendl_fd("Impossible d'initialiser le garrray.", 2);
		return (-1);
	}
	/* Test of set cells */
	ft_putendl("[Test] Set value to cells : \n");
	i = 0;
	while (i < array->size)
	{
		array->setvalue(array, i, string, ft_strlen(string));
		array->clear(array, 8, 0);
		(!(array->cells[i]->content)) ? ft_putstr("(null)") : ft_putstr((char *)(array->cells[i]->content));
		ft_putchar('\n');
		i++;
	}
	/* Unit test of setvalue */
	ft_putendl("[Test] Function set value :\nIndex: 8");
	array->setvalue(array, 8, &magic, sizeof(magic));
	ft_putnbr(*((int *)(array->cells[i - 1]->content)));
	ft_putchar('\n');

	ft_putendl("[Test] Function set value :");
	ft_putnbr(array->getlength(array));
	array->free(array);
	array = NULL;
	free(string);
	ft_putchar('\n');
	/*
	 * Set define LEAKS at 1 if u want use ps -> leaks.
	 */
	if (LEAKS)
	{
		ft_putendl("Leaks");
		while (1)
			;
	}
	return (0);
}
