/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   generic_array.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/03 23:51:21 by tiboitel          #+#    #+#             */
/*   Updated: 2014/12/04 04:11:58 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "generic_array.h"

t_garray	*garray_new(size_t size)
{
	t_garray	*array;
	size_t		i;

	if (!(array = (t_garray *)ft_memalloc(sizeof(t_garray))))
		return (NULL);
	if (!(array->cells = (t_gcell **)ft_memalloc(sizeof(t_gcell) * size)))
		return (NULL);
	i = -1;
	array->size = size;
	while (++i < array->size)
	{
		if (!(array->cells[i] = (t_gcell *)ft_memalloc(sizeof(t_gcell))))
			return (NULL);
		array->cells[i]->content = NULL;
		array->cells[i]->content_size = 0;
	}
	array->getlength = garray_getlength;
	array->setvalue = garray_setvalue;
	array->clear = garray_clear;
	array->free = garray_free;
	return (array);
}

size_t		garray_getlength(t_garray *array)
{
	return (((!array) ? 0 : array->size));
}

int			garray_setvalue(t_garray *array, size_t index, void *content,\
		size_t content_size)
{
	if (!array)
		return (-1);
	if (array->cells[index])
	{
		free(array->cells[index]->content);
		if (!(array->cells[index]->content = ft_memalloc(content_size)))
		{
			ft_putendl_fd("Unable to allocate memory for cell.", 2);
			return (-1);
		}
		array->cells[index]->content_size = content_size;
		ft_memcpy(array->cells[index]->content, content, content_size);
	}
	return (1);
}

void		garray_clear(t_garray *array, size_t index, size_t length)
{
	size_t	i;

	if (!array || !index)
		return ;
	i = index;
	if (!length)
	{
		free(array->cells[i]->content);
		array->cells[i]->content = NULL;
		array->cells[i]->content_size = 0;
	}
	else
	{
		while (i < length)
		{
			free(array->cells[i]->content);
			array->cells[i]->content = NULL;
			array->cells[i]->content_size = 0;
			i++;
		}
	}
}

void		garray_free(t_garray *array)
{
	size_t	i;

	if (!array || array->size == 0)
		return ;
	i = -1;
	while (++i < array->size)
	{
		free(array->cells[i]->content);
		array->cells[i]->content = NULL;
		array->cells[i]->content_size = 0;
		free(array->cells[i]);
	}
	free(array->cells);
	free(array);
}
