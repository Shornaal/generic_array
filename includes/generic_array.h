/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   generic_array.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/03 23:51:38 by tiboitel          #+#    #+#             */
/*   Updated: 2014/12/04 04:12:00 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_GENERIC_ARRAY_H
# define FT_GENERIC_ARRAY_H
# include <libft.h>

typedef struct	s_gcell
{
	void	*content;
	size_t	content_size;
}				t_gcell;

typedef struct	s_garray
{
	t_gcell	**cells;
	size_t	size;
	int		(*setvalue)(struct s_garray *, size_t index, void *content, \
			size_t content_size);
	size_t	(*getlength)(struct s_garray *);
	void	(*clear)(struct s_garray *, size_t index, size_t length);
	void	(*free)(struct	s_garray *);
}				t_garray;

t_garray		*garray_new(size_t size);
int				garray_setvalue(t_garray *array, size_t index, void *content, \
		size_t content_size);
size_t			garray_getlength(t_garray *array);
void			garray_clear(t_garray *array, size_t index, size_t length);
void			garray_free(t_garray *array);

#endif
